module podmanbootc

go 1.19

require (
	github.com/fsnotify/fsnotify v1.7.0
	github.com/spf13/cobra v1.8.0
	github.com/coreos/stream-metadata-go v0.4.4
	github.com/adrg/xdg v0.4.0
	github.com/inconshreveable/mousetrap v1.1.0 // indirect
	github.com/sirupsen/logrus v1.9.3
	github.com/spf13/pflag v1.0.5 // indirect
	golang.org/x/sys v0.4.0
)
